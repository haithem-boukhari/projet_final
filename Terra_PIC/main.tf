resource "azurerm_resource_group" "rg" {
    name= "${var.name_rg}"
    location = "${var.location}"

    tags {
        owner= "${var.owner}"
    }
}
resource "azurerm_virtual_network" "vNet" {
    name= "${var.name_vnet}"
    address_space= ["${var.address_space}"]
    location= "${azurerm_resource_group.rg.location}"
    resource_group_name= "${azurerm_resource_group.rg.name}"  
}

resource "azurerm_subnet" "subNet" {
    name= "${var.name_subNet}"
    resource_group_name= "${azurerm_resource_group.rg.name}" 
    virtual_network_name= "${azurerm_virtual_network.vNet.name}"
    address_prefix= "${var.address_prefix}"
}

resource "azurerm_network_security_group" "NSecure" {
  name= "${var.name_secure}"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  

    security_rule {
        name= "SSH"
        priority= "1001"
    #incrémenter à chaque création de port"
        direction= "Inbound"
        access= "Allow"
        protocol= "Tcp"
        source_port_range= "*"
        destination_port_range= "22"
        source_address_prefix= "*"
        destination_address_prefix= "*"
    }
    security_rule {
        name= "HTTP"
        priority= "1002"
        direction= "Inbound"
        access= "Allow"
        protocol= "Tcp"
        source_port_range= "*"
        destination_port_range= "80"
        source_address_prefix= "*"
        destination_address_prefix= "*"
    }

    security_rule {
        name= "jenkins"
        priority= "1003"
        direction= "Inbound"
        access= "Allow"
        protocol= "Tcp"
        source_port_range= "*"
        destination_port_range= "8080"
        source_address_prefix= "*"
        destination_address_prefix= "*"
    }  
}

resource "azurerm_public_ip" "IP" {
  name= "${var.name_IP}"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  allocation_method= "Static"
  domain_name_label= "${var.name_DNS}"
}

resource "azurerm_network_interface" "NICMaster" {
  name= "${var.name_ni}_master"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}" 
  network_security_group_id= "${azurerm_network_security_group.NSecure.id}"

  ip_configuration {
    name= "${var.ip_config}_master"
    subnet_id= "${azurerm_subnet.subNet.id}"
    private_ip_address_allocation = "Static"
    private_ip_address= "10.0.1.94"
  }
}

resource "azurerm_network_interface" "NICSlave" {
  name= "${var.name_ni}_slave"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}" 
  network_security_group_id= "${azurerm_network_security_group.NSecure.id}"

  ip_configuration {
    name= "${var.ip_config}_slave"
    subnet_id= "${azurerm_subnet.subNet.id}"
    private_ip_address_allocation = "Static"
    private_ip_address= "10.0.1.88"
  }
}

resource "azurerm_network_interface" "NICReverse" {
  name= "${var.name_ni}_Reverse"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}" 
  network_security_group_id= "${azurerm_network_security_group.NSecure.id}"

  ip_configuration {
    name= "${var.ip_config}_Reverse"
    subnet_id= "${azurerm_subnet.subNet.id}"
    private_ip_address_allocation = "Static"
    private_ip_address= "10.0.1.77"
    public_ip_address_id= "${azurerm_public_ip.IP.id}"
  }
}

resource "azurerm_virtual_machine" "Master" {
  name= "Master"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  network_interface_ids= [ "${azurerm_network_interface.NICMaster.id}" ]
  vm_size= "Standard_B2s"

  storage_image_reference {
    publisher= "OpenLogic"
    offer= "CentOS"
    sku= "7.5"
    version= "latest"
  }

  storage_os_disk {
    name= "osdisk_Master"
    caching= "ReadWrite"
    create_option= "FromImage"
    managed_disk_type= "Standard_LRS"
  }

  os_profile {
    computer_name= "Master"
    admin_username= "Master"
  }

  os_profile_linux_config {
      disable_password_authentication= true

      ssh_keys {
          path= "/home/Master/.ssh/authorized_keys"
          key_data= "${var.ssh_pub}"
      }
  }
}

resource "azurerm_virtual_machine" "Slave" {
  name= "Slave"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  network_interface_ids= [ "${azurerm_network_interface.NICSlave.id}" ]
  vm_size= "${var.vmSize}"

  storage_image_reference {
    publisher= "OpenLogic"
    offer= "CentOS"
    sku= "7.5"
    version= "latest"
  }

  storage_os_disk {
    name= "osdisk_Slave"
    caching= "ReadWrite"
    create_option= "FromImage"
    managed_disk_type= "Standard_LRS"
  }

  os_profile {
    computer_name= "Slave"
    admin_username= "Slave"
  }

  os_profile_linux_config {
      disable_password_authentication= true

      ssh_keys {
          path= "/home/Slave/.ssh/authorized_keys"
          key_data= "${var.ssh_pub}"
      }
  }
}

resource "azurerm_virtual_machine" "Reverse" {
  name= "Reverse"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  network_interface_ids= [ "${azurerm_network_interface.NICReverse.id}" ]
  vm_size= "${var.vmSize}"

  storage_image_reference {
    publisher= "OpenLogic"
    offer= "CentOS"
    sku= "7.5"
    version= "latest"
  }

  storage_os_disk {
    name= "osdisk_Reverse"
    caching= "ReadWrite"
    create_option= "FromImage"
    managed_disk_type= "Standard_LRS"
  }

  os_profile {
    computer_name= "Reverse"
    admin_username= "Reverse"
  }

  os_profile_linux_config {
      disable_password_authentication= true

      ssh_keys {
          path= "/home/Reverse/.ssh/authorized_keys"
          key_data= "${var.ssh_pub}"
      }
  }
}
# resource "azurerm_container_registry" "CR" {
#   name= "${var.name_CR}"
#   resource_group_name= "${azurerm_resource_group.rg.name}" 
#   location= "${azurerm_resource_group.rg.location}"
#   sku= "standard"
#   admin_enabled= false
# }

# resource "azurerm_container_group" "CGPIC" {
#   name= "${var.name_CG}"
#   resource_group_name= "${azurerm_resource_group.rg.name}" 
#   location= "westeurope"
#   ip_address_type= "${var.IP_type}"
#   os_type= "${var.OS_type}"

#   container {
#     name   = "ansible"
#     image  = "cytopia/ansible:latest"
#     cpu    = "1"
#     memory = "1.5"

#     ports {
#       port= "9494"
#       protocol= "TCP"
#     }
#   }

#     container {
#     name   = "terraform"
#     image  = "hashicorp/terraform:0.11.11"
#     cpu    = "1"
#     memory = "1.5"

#     ports {
#       port= "8888"
#       protocol= "TCP"
#     }
#   }
# }