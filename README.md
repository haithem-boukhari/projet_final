**Tartan Project**
==================

Tartan propose la livraison d'une application à travers :
- Une Plateforme d'Intégration Continue qui embarque Jenkins, Maven, Docker-Engine, Terraform et Ansible;
- Une Pile Complète de déploiements composée de 2 serveurs de Tests et de Production JAVA 12 et les bases de données MongoDB nécessaires au bon fonctionnement de l'application déployée.

**Pré-requis logiciels**
-------------------------

- VirtualBox et Vagrant [vérifier que les versions soient compatibles ou utiliser les versions de développement par défaut (cf.*Développé avec*)]
- Git

**Installation de base**
-------------------------
Sur l'OS du poste de travail, exécuter la commande: 

```
git clone https://gitlab.com/edegouys/projet_final.git
```
!!(Merci de forker le projet sur votre propre compte GitLab avant d'effectuer des commits)!!

Tartan embarque une machine virtuelle Vagrant Centos7 qui permet l'installation de la PIC.
La VM embarque nativement GIT et Ansible.

Dans le répertoire VM_base, ouvrir un terminal et lancer les commandes: 

```
vagrant up
vagrant ssh
```
Dans le répertoire d'accueil de la VM, lancer la commande:

```
git clone https://gitlab.com/edegouys/projet_final.git
```
Générer une paire de clefs SSH dans le répertoire .ssh en laissant les noms proposés par défaut et sans ajouter de passphrase:

```
ssh-keygen
```
Dans les dossiers Terra-PIC et Terra-PILE du répertoire projet final, créer un dossier *secrets* contenant vos identifiants de connexion au cloud Azure ainsi que la clef publique (id_rsa.pub) générée:

```
mkdir secrets
cd secrets
touch backend.tfvars
vi backend.tfars
```

Les lignes du fichier sont les suivantes:

`subscription_id=` ""<br>
`client_id=` ""<br>
`client_secret=` ""<br>
`tenant_id=` ""<br>
`ssh_pub=` ""<br>

Pour connaître les différents ID Azure, utiliser les commandes suivantes après avoir installé Azure CLI 2.0

```
az login
az aks get-credentials 
```
Effectuer un 

```
git add .
git commit -m "màj secrets"
git push
```

**Installation de l'infrastructure de la PIC**
-----------------------------------------------

Se placer dans le répertoire des rôles Ansible

```
cd /projet_final/ROLES_ANSIBLE
```
Depuis ce répertoire, lancer le provisionnement de la machine locale qui permettra de configurer les hôtes SSH et d'installer Terraform

```
ansible-plabook Install_LOCAL.yml
```
Se déplacer ensuite dans

```
cd /projet_final/Terra dans Terra-PIC
```
puis lancer les commandes suivantes pour provisionner l'infrastructure de la Plateforme d'Intégration Continue

```
terraform init
terraform plan -var-file="secrets/backend.tfvars" -var-file="variables/main.tfvars"
terraform apply -var-file="secrets/backend.tfvars" -var-file="variables/main.tfvars"
```
L'infrastructure créée, il faut la provisionner en outils à l'aide des commandes suivantes:

```
cd /projet_final/ROLES_ANSIBLE
ansible-plabook Install_PIC.yml -i inventory
```

Une fois l'infrastructure provisionnée, l'accès à jenkins se fait à l'URL suivante:

[http://team-tartan.francecentral.cloudapp.azure.com/]

Il faut alors paramétrer le compte admin de Jenkins et lancer l'installation des plugins de base.<br>

**Création du Slave Maven/Docker-Engine**
-------------------------------------------

Une fois sur la page d'accueil de Jenkins, aller sur "Administrer Jenkins" 

1.  "Configurer les identifiants"

Paramétrer comme suit :

`Portée` - Global<br>
`ID` - ssh_slave<br>
`Description` - ssh_slave<br>
`Username` - Slave<br>
`Private Key` - copier le contenu de id_rsa<br>
`Passphrase` - Laisser vide<br>

2.  "Gérer les noeuds" et enfin "Créer un noeud".

Paramétrer comme suit :

`Nom` - Slave<br>
`Description` - Noeud Maven/Docker-Engine<br>
`Nb d'exécuteurs` - 1<br>
`Répertoire de travail du système distant` - /home/Slave<br>
`Étiquettes` - Slave<br>
`Utilisation` - Utiliser ce noeaud autant que possible <br>
`Méthode de lancement` - Launch agents via SSH<br>
    - `Host` - 10.0.1.88<br>
    - `Credentials`- Slave (ssh_slave)<br>
    - `Host Key Verification Strategy` - Non Host Key Verification Strategy<br>
    
3.  "Gestion des plugins" 

Ajouter les plugins Docker, Ansible, Git et Maven puis redémarrer Jenkins.

**Création les jobs Pipeline Jenkins**
-------------------------------------

Depuis l'écran d'accueil de Jenkins, créer un nouvel item de type Pipeline.<br>

1. Création des containers de la PIC

Le premier job permet de créer 2 containers, Terraform et Ansible, rattachés au Slave.
Dans la définition de la Pipeline, choisir **Pipeline script from SCM** puis:<br>
`SCM` Git<br>
`Repositories` https://gitlab.com/edegouys/projet_final.git<br>
`Script Path` Jenkinsfile_images 
Laisser les autres paramètres par défaut.<br>

1. Création de l'infrastructure de la Pile Complète et déploiement de l'application

Le second job est le job relatif à la Pile Complète de tests et déploiement de l'application.
Dans la définition de la Pipeline, choisir **Pipeline script from SCM** puis:<br>
`SCM` Git<br>
`Repositories` https://gitlab.com/edegouys/projet_final.git<br>
`Script Path` Jenkinsfile
Laisser les autres paramètres par défaut.<br>

Dans **Build Triggers**, paramétrer le WEB Hook pour que le job se lance automatiquement à chaque push sur le repo de l'application, que ce soit sur la branche Master (build sur l'environnement de production) ou Develop (build sur l'environnement de tests).

**URLs**
---------

L'application déployée sur l'environnement de production à partir de la branche Master est visible à l'url [http://team-tartan.francecentral.cloudapp.azure.com/prod]<br>
L'application déployée sur l'environnement de tests à partir de la branche Develop est visible à l'url [http://team-tartan.francecentral.cloudapp.azure.com/test]

**Développé avec**
-------------------

- Windows 10 Pro 
- Virtual Box 6.0.14 et Vagrant 2.2.6 pour les machines virtuelles
- Visual Studio Code 1.41.1
- Azure Cloud
- Terraform 0.11.11 
- Ansible 2.4.2.0
- Trello - Gestion de projet : https://trello.com/b/Ht23G8Q2/projet-final
- GitLab - Source Controle Manager: 
    - Repo du projet : https://gitlab.com/edegouys/projet_final/
    - Repo App Test : https://gitlab.com/edegouys/Restful-Webservice (forked from @girldevops on GitHub)


**Auteurs**
----------------

Pour la Team Tartan : <br>
Sebastian Castaño Palacio @NekoTorll - nekotorll0123@gmail.com <br>
Estelle Degouys @edegouys - estelle.degouys@gmail.com